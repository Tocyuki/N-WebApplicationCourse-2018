(function() {
    'use strict';
    var myBirthTime = new Date(1985, 4, 10, 19, 30);
    function updateParagraph() {
        var now = new Date();
        var days = ((now.getTime() - myBirthTime.getTime())) / (1000 * 3600 * 24);
        document.getElementById('birth-time').innerText = '生まれてから' + Math.floor(days) + '日経過。';
    }
    setInterval(updateParagraph, 1000);
})();